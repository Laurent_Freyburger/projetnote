
//
//  AddEditNoteTableViewController.swift
//  projetNote
//
//  Created by freyburger laurent on 06/03/2019.
//  Copyright © 2019 freyburger laurent. All rights reserved.
//

import UIKit
import MapKit

class AddEditNoteTableViewController: UITableViewController {
    
    var note: Note?
    
    @IBOutlet weak var titleTextField: UITextField!
    
    @IBOutlet weak var contentTextField: UITextField!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    var locationManager : CLLocationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let note = note {
            
            titleTextField.text = note.titre
            contentTextField.text = note.contenu
            let location = CLLocationCoordinate2D(latitude:note.longitude , longitude:  note.latitude)
            let span = MKCoordinateSpanMake(0.5,0.5)
            let region = MKCoordinateRegion(center:location,span:span)
            map.setRegion(region, animated: true)
            let annotation = MKPointAnnotation()
            annotation.coordinate = location
            map.addAnnotation(annotation)
            
            map.selectAnnotation(annotation, animated: true)
           
          
        }
        updateSaveButtonState()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    func updateSaveButtonState() {
        let titleText = titleTextField.text ?? ""
        let contentText = contentTextField.text ?? ""
        saveButton.isEnabled = !titleText.isEmpty && !contentText.isEmpty
    }

    @IBAction func textEditingChange(_ sender: UITextField ) {
        updateSaveButtonState()
    }
    
    
    // MARK: - Table view data source

//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//       return 4
//    }
////
//   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//       // #warning Incomplete implementation, return the number of rows
//      return 4
//  }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "SaveUnwind" {
            let title = titleTextField.text ?? ""
            let content = contentTextField.text ?? ""
           let currentDateTime = Date()
            let calendar = Calendar.current
            let components = calendar.dateComponents([.year,.month,.day] , from: currentDateTime)
            
            let dateT = "\(components.year ?? 0)  -  \( components.month ?? 0)  - \( components.day ?? 0) "
           
            let clocation = map.annotations.last?.coordinate
            note = Note(titre: title, contenu: content, dateCreation: dateT,  latitude:clocation?.longitude ?? 0.0, longitude:clocation?.longitude ?? 0.0)
        }
        
    }

}
