//
//  TableViewController.swift
//  projetNote
//
//  Created by freyburger laurent on 08/02/2019.
//  Copyright © 2019 freyburger laurent. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
 
    var Notes:[Note] = [
        Note(titre:"note1",contenu:"content1",dateCreation: "1996-12-19" , latitude:7.335498,longitude:47.750501),
        Note(titre:"note2",contenu:"content2",dateCreation:"1996-12-19" , latitude:7.335498,longitude:47.750501),
        Note(titre:"note3",contenu:"content3",dateCreation:"1996-12-19" , latitude:7.335498,longitude:47.750501)
    
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
         self.navigationItem.leftBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return Notes.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellsName", for: indexPath)
        let note = Notes[indexPath.row]
        cell.textLabel?.text = note.titre
        cell.detailTextLabel?.text = note.dateCreation.description
        cell.backgroundView = UIView()
        cell.showsReorderControl = true
        // Configure the cell...

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            Notes.remove(at: indexPath.row)
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let note = Notes.remove(at:fromIndexPath.row)
        Notes.insert(note,at:to.row)
        tableView.reloadData()
    }
    

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func unwindToNoteTableView(segue: UIStoryboardSegue) {
        
        if segue.identifier == "SaveUnwind" {
            let sourceVC = segue.source as! AddEditNoteTableViewController
            
            if let note = sourceVC.note {
                if let selectedIndexPath = tableView.indexPathForSelectedRow {
                    // update
                    Notes[selectedIndexPath.row] = note
                    tableView.reloadRows(at: [selectedIndexPath], with: .none)
                } else { // insert
                    let newIndexPath = IndexPath(row: Notes.count, section: 0)
                    Notes.append(note)
                    tableView.insertRows(at: [newIndexPath], with: .automatic)
                    
                }
            }
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "EditNote" {
            let indexPath = tableView.indexPathForSelectedRow!
            let note = Notes[indexPath.row]
            let navigationController = segue.destination as! UINavigationController
            let addEditController = navigationController.topViewController as! AddEditNoteTableViewController
            addEditController.note = note
            
        }else{
            let navigationController = segue.destination as! UINavigationController
            let addEditController2 = navigationController.topViewController as! AddEditNoteTableViewController
            let date = Date()
            let calendar = Calendar.current
            let components = calendar.dateComponents([.year,.month,.day] , from: date)
           
            let dateT = "\(components.year ?? 0)  -  \( components.month ?? 0)  - \( components.day ?? 0) "
    
            let note2 = Note(titre: "", contenu: "", dateCreation: dateT, latitude:7.335498 , longitude: 47.750501)
            addEditController2.note = note2
        }
    }
    

}
