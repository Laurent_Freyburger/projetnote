//
//  Note.swift
//  projetNote
//
//  Created by freyburger laurent on 08/02/2019.
//  Copyright © 2019 freyburger laurent. All rights reserved.
//

import Foundation


class Note{
    var titre:String
    var contenu:String
    var dateCreation:Date
    var longitude:Double
    var latitude:Double
    
    init(titre:String,contenu:String,dateCreation:String, latitude:Double,longitude:Double){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: dateCreation)
        self.titre = titre
        self.contenu = contenu
        self.dateCreation = date!
        self.longitude = longitude
        self.latitude = latitude
        
    }
}
